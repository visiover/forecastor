# Create a virtual environment to isolate our package dependencies locally
`python3 -m venv env`
`source env/bin/activate`

# Install Django and Django REST framework into the virtual environment
`pip3 install django`
`pip3 install djangorestframework`
`pip3 install markdown`

# Fire up the server from the command line
`python3 manage.py runserver`